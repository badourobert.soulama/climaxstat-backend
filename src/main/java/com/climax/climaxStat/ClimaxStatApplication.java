package com.climax.climaxStat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClimaxStatApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClimaxStatApplication.class, args);
	}

}
