package com.climax.climaxStat.controllers;

import com.climax.climaxStat.enumerations.FileType;
import com.climax.climaxStat.model.Client;
import com.climax.climaxStat.readers.FileReader;
import com.climax.climaxStat.readers.FileReaderFactory;
import com.climax.climaxStat.services.StatisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class FileController {

    private final StatisticsService statisticsService;
    private final FileReaderFactory fileReaderFactory;

    @PostMapping("/upload")
    @CrossOrigin(origins = "http://localhost:4200")
    public Map<String, Double> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        FileReader fileReader = fileReaderFactory.getInstance(FileType.fromContentType(file.getContentType()));
        List<Client> clients = fileReader.read(file.getInputStream());
        for (Client client : clients) {
            statisticsService.addClient(client);
        }
        return statisticsService.calculateAverageSalaryByProfession();
    }
}
