package com.climax.climaxStat.readers;

import com.climax.climaxStat.model.Client;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVFileReader implements FileReader{

    @Override
    public List<Client> read(InputStream inputStream) {
        List<Client> clients = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            CSVParser csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);

            for (CSVRecord csvRecord : csvParser) {
                String nom = csvRecord.get("nom");
                String prenom = csvRecord.get("prenom");
                int age = Integer.parseInt(csvRecord.get("age"));
                String profession = csvRecord.get("profession");
                double salaire = Double.parseDouble(csvRecord.get("salaire"));

                Client client = new Client(nom, prenom, age, profession, salaire);
                clients.add(client);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return clients;
    }
}
