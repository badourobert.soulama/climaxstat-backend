package com.climax.climaxStat.readers;

import com.climax.climaxStat.enumerations.FileType;
import org.springframework.stereotype.Component;

@Component
public class FileReaderFactory {
    public static FileReader getInstance(FileType fileType) {
        switch (fileType) {
            case TEXT:
                return new TextFileReader();
            case CSV:
                return new CSVFileReader();
            case XML:
                return new XMLFileReader();
            case JSON:
                return new JSONFileReader();
            default:
                throw new IllegalArgumentException("Type de fichier non supporté: " + fileType);
        }
    }
}

