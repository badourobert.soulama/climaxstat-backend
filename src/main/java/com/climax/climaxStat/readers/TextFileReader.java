package com.climax.climaxStat.readers;

import com.climax.climaxStat.model.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TextFileReader implements FileReader {
    @Override
    public List<Client> read(InputStream inputStream) {
        List<Client> clients = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {

                line = line.replaceAll(";$", "").trim();

                String[] fields = line.split(",");

                if (fields.length == 5) {
                    try {
                        String nom = fields[0].trim();
                        String prenom = fields[1].trim();
                        int age = Integer.parseInt(fields[2].trim());
                        String profession = fields[3].trim();
                        double salaire = Double.parseDouble(fields[4].trim());

                        Client client = new Client(nom, prenom, age, profession, salaire);
                        clients.add(client);
                    } catch (NumberFormatException e) {
                        System.out.println("Erreur de format numérique dans la ligne: " + line);
                    }
                } else {
                    System.out.println("Ligne avec un format incorrect : " + line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clients;
    }
}
