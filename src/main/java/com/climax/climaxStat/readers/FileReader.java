package com.climax.climaxStat.readers;

import com.climax.climaxStat.model.Client;

import java.io.InputStream;
import java.util.List;

public interface FileReader {

    List<Client> read(InputStream inputStream);
}
