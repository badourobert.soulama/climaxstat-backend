package com.climax.climaxStat.readers;

import com.climax.climaxStat.model.Client;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.InputStream;
import java.util.List;

public class JSONFileReader implements FileReader {

    private final ObjectMapper objectMapper;

    public JSONFileReader() {
        this.objectMapper = new ObjectMapper();
    }
    @Override
    public List<Client> read(InputStream inputStream) {
        try {
            return objectMapper.readValue(inputStream, new TypeReference<List<Client>>() {});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
