package com.climax.climaxStat.readers;

import com.climax.climaxStat.model.Client;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XMLFileReader implements FileReader {
    @Override
    public List<Client> read(InputStream inputStream) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputStream);
            document.getDocumentElement().normalize();

            List<Client> clients = new ArrayList<>();
            NodeList nodeList = document.getElementsByTagName("client");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element element = (Element) nodeList.item(i);
                String nom = element.getElementsByTagName("nom").item(0).getTextContent();
                String prenom = element.getElementsByTagName("prenom").item(0).getTextContent();
                int age = Integer.parseInt(element.getElementsByTagName("age").item(0).getTextContent());
                String profession = element.getElementsByTagName("profession").item(0).getTextContent();
                int salaire = Integer.parseInt(element.getElementsByTagName("salaire").item(0).getTextContent());
                clients.add(new Client(nom, prenom, age, profession, salaire));
            }
            return clients;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
