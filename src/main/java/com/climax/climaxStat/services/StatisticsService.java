package com.climax.climaxStat.services;

import com.climax.climaxStat.model.Client;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatisticsService {

    private Map<String, List<Double>> salaryByProfession = new HashMap<>();

    public void addClient(Client client) {
        salaryByProfession.computeIfAbsent(client.getProfession(), k -> new ArrayList<>()).add(client.getSalaire());
    }

    public Map<String, Double> calculateAverageSalaryByProfession() {
        Map<String, Double> averageSalaryByProfession = new HashMap<>();
        salaryByProfession.forEach((profession, salaries) -> {
            double average = salaries.stream().mapToDouble(Double::doubleValue).average().orElse(0);
            averageSalaryByProfession.put(profession, average);
        });
        return averageSalaryByProfession;
    }
}
