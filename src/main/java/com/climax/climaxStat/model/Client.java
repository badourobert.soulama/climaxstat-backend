package com.climax.climaxStat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {
   private String nom;
   private String prenom;
   private int age;
   private String profession;
   private double salaire;
}
