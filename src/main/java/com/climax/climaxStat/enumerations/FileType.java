package com.climax.climaxStat.enumerations;

public enum FileType {
    CSV("text/csv"),
    XML("text/xml"),
    JSON("application/json"),
    TEXT("text/plain");

    private final String contentType;

    FileType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public static FileType fromContentType(String contentType) {
        for (FileType fileType : values()) {
            if (fileType.contentType.equals(contentType)) {
                return fileType;
            }
        }
        throw new IllegalArgumentException("Type de contenu non supporté: " + contentType);
    }
}
